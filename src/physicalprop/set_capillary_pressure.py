import numpy as np
from numba import jit
from compass_utils import messages
from physicalprop.utils import load_library
from physicalprop.properties import BothProperties


def set_capillary_pressure_functions(
    rock_physical_prop,
    rocktype_id=0,
    functions=None,
    *,
    check_derivatives=True,
    library=None,
):
    """
    Set the capillary pressure functions (with and without the derivatives)
    for all phases Palpha = Pref - Pc^alpha (because Pref is gas pressure).
    If no function is given, use default values.
    To use another function, give the function
    :param rocktype: (optional) rocktype identifier
    :param functions: (optional) list of BothProperties (nb_phases -1 BothProperties)
    :param check_derivatives: (True by default) check the derivatives of the property
    """
    if functions is not None:
        messages.warning("overriding default capillary pressure functions")
        functions = build_pref2palpha_from_pc(
            functions, rock_physical_prop.physics, library
        )
    else:
        functions = get_default_pc_functions(rock_physical_prop.physics, library)

    register_c_property_module = BothProperties(
        with_derivatives=rock_physical_prop.cpp_prop_with_derivatives.register_c_pc_with_derivatives,
        without_derivatives=rock_physical_prop.cpp_prop_without_derivatives.register_c_pc_without_derivatives,
    )
    rock_physical_prop.register_property(
        "capillary_pressure",
        rocktype_id,
        functions,
        register_c_property_module,
        check_derivatives,
    )


def get_default_pc_functions(the_physics, library):
    lib = load_library(library, "capillary_pressure")

    if the_physics.nb_phases == 1:
        return lib.no_capillary_pressure  # Pg = Pref + 0

    elif the_physics.nb_phases == 2:
        return [
            lib.no_capillary_pressure,  # Pg = Pref + 0
            lib.no_capillary_pressure,  # Pl = Pref + 0
        ]

    else:
        raise "Capillary pressure not implemented for this physics"


def build_pref2palpha_from_pc(pc, physics, library):
    assert not isinstance(pc, (list, np.ndarray))
    lib = load_library(library, "capillary_pressure")

    # Pl = Pref + pref2pliq with Pref = Pg
    # thus pref2pliq = Pl - Pg = - Pc
    pref2pgas = lib.no_capillary_pressure

    compiled_pc_with_derivatives = jit(pc.with_derivatives, nopython=True)
    compiled_pc_without_derivatives = jit(pc.without_derivatives, nopython=True)

    def pref2pliq_without_derivatives(X):
        return -compiled_pc_without_derivatives(X)

    def pref2pliq_with_derivatives(X, ddfX):
        val = compiled_pc_with_derivatives(X, ddfX)
        ddfX.saturation *= -1
        return -val

    pref2pliq = BothProperties(
        with_derivatives=pref2pliq_with_derivatives,
        without_derivatives=pref2pliq_without_derivatives,
    )
    assert physics.Phase.gas == 0
    assert physics.Phase.liquid == 1
    return [pref2pgas, pref2pliq]
