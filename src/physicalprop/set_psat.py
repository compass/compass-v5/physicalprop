from compass_utils import messages
from physicalprop.utils import load_library
from physicalprop.properties import BothProperties


def set_psat_function(
    physical_prop,
    function=None,
    *,
    check_derivatives=True,
    library=None,
):
    """
    Set the psat function (with and without the derivatives).
    If no psat function is given, use default value.
    To use another function, give the function
    :param function: (optional) one function
    :param check_derivatives: (True by default) check the derivatives of the property
    """
    if function is not None:
        messages.warning("overriding default psat function")
    else:
        function = get_default_psat_function(physical_prop.physics, library)

    register_c_property_module = BothProperties(
        with_derivatives=physical_prop.cpp_prop_with_derivatives.register_c_psat_with_derivatives,
        without_derivatives=physical_prop.cpp_prop_without_derivatives.register_c_psat_without_derivatives,
    )
    physical_prop.register_psat(
        function,
        register_c_property_module,
        check_derivatives,
    )


def get_default_psat_function(the_physics, library):
    lib = load_library(library, "psat")
    physics_name = the_physics.name()

    if physics_name == "diphasic":
        return lib.diphasic_psat

    # elif physics_name == "immiscible2ph":
    #     return [
    #         lib.gas_immiscible2ph_molar_enthalpies,
    #         lib.liquid_diphasic_molar_enthalpies,
    #     ]

    # elif physics_name == "water2ph":
    #     return [lib.gas_water2ph_enthalpies, lib.liquid_water2ph_enthalpies]

    elif physics_name == "linear_water":
        return lib.error_psat

    # elif physics_name == "brine":
    #     return lib.brine_enthalpies

    else:
        raise "psat not implemented for this physics"
