#pragma once
#include <nanobind/nanobind.h>
#include <physicalprop/physicalprop.h>
#include <physics/model.h>

// move this file into the diphasic physics

namespace physics {
namespace nb = nanobind;

inline void add_specific_model_wrappers(nb::module_ &module) {

  module.def(
      "build_state",
      [](physical_prop::FMP_d &fluid_properties,
         physical_prop::RP_d &rock_properties, int context, nb::object p,
         nb::object T, nb::object Sg, nb::object Cag, nb::object Cal,
         nb::object rocktype) {
        //  constexpr auto gas = enum_to_rank(Phase::gas);
        //  constexpr auto liquid = enum_to_rank(Phase::liquid);
        //  constexpr auto air = enum_to_rank(Component::air);
        //  constexpr auto water = enum_to_rank(Component::water);

        auto set_gas_state = [&](State &state) {
          if (p.is_none())
            throw std::runtime_error(
                "You need to provide pressure for diphasic state.");
          if (T.is_none())
            throw std::runtime_error(
                "You need to provide temperature for diphasic state.");
          if (!Sg.is_none())
            throw std::runtime_error(
                "You dont need to provide saturation for gas state.");
          if (!Cal.is_none())
            throw std::runtime_error(
                "You dont need to provide liquid molar fractions for gas "
                "state.");
          state.pressure[Phase::gas] = nb::cast<double>(p);
          state.pressure[Phase::liquid] = state.pressure[Phase::gas]; // ???
          // other pressures ???
          state.temperature = nb::cast<double>(T);
          state.saturation.fill(0);
          state.saturation[Phase::gas] = 1;
          state.molar_fractions[Phase::gas][Component::air] =
              Cag.is_none() ? 1. : nb::cast<double>(Cag);
          state.molar_fractions[Phase::liquid][Component::air] = 0;
          enforce_consistent_molar_fractions(state.molar_fractions);
        };

        auto set_liquid_state = [&](State &state) {
          if (p.is_none())
            throw std::runtime_error(
                "You need to provide pressure for diphasic state.");
          if (T.is_none())
            throw std::runtime_error(
                "You need to provide temperature for diphasic state.");
          if (!Sg.is_none())
            throw std::runtime_error(
                "You dont need to provide saturation for liquid state.");
          if (!Cag.is_none())
            throw std::runtime_error(
                "You dont need to provide gas molar fractions for liquid "
                "state.");
          state.pressure[Phase::gas] = nb::cast<double>(p);
          state.pressure[Phase::liquid] = state.pressure[Phase::gas]; // ???
          state.temperature = nb::cast<double>(T);
          state.saturation.fill(0);
          state.saturation[Phase::liquid] = 1;
          state.molar_fractions[Phase::gas][Component::air] = 1;
          state.molar_fractions[Phase::liquid][Component::air] =
              Cal.is_none() ? 0 : nb::cast<double>(Cal);
          enforce_consistent_molar_fractions(state.molar_fractions);
        };

        auto set_diphasic_state = [&](State &state) {
          if (p.is_none())
            throw std::runtime_error(
                "You need to provide pressure for diphasic state.");
          if (T.is_none())
            throw std::runtime_error(
                "You need to provide temperature for diphasic state.");
          if (Sg.is_none())
            throw std::runtime_error(
                "You need to provide saturation for diphasic state.");
          if (!(Cag.is_none() && Cal.is_none()))
            throw std::runtime_error(
                "Don't prescribe molar fractions for diphasic contexts.");
          state.pressure[Phase::gas] = nb::cast<double>(p);
          state.temperature = nb::cast<double>(T);
          state.saturation[Phase::gas] = nb::cast<double>(Sg);
          state.saturation[Phase::liquid] = 1. - state.saturation[Phase::gas];
          if (rocktype.is_none()) {
            state.pressure[Phase::liquid] = state.pressure[Phase::gas]; // ???
            //  update_phase_pressures(state);
          } else {
            auto &&Pg2Pl = rock_properties.pref2palpha(nb::cast<int>(rocktype),
                                                       Phase::liquid);
            auto &&x = Xsat{};
            for (auto &&alpha : all_phases::array)
              x.saturation[alpha] = state.saturation[alpha];
            state.pressure[Phase::liquid] =
                state.pressure[Phase::gas] + Pg2Pl(x).value;
          }
          state.molar_fractions = diphasic_equilibrium(
              physical_prop::_init_alpha_fugacity_functors(fluid_properties),
              state.pressure, state.temperature);
        };

        Context context_value = static_cast<Context>(context);
        State result;
        switch (context_value) {
        case Context::gas:
          set_gas_state(result);
          break;
        case Context::liquid:
          set_liquid_state(result);
          break;
        case Context::diphasic:
          set_diphasic_state(result);
          break;
        default:
          throw std::runtime_error("Requested context does not exist!");
        }
        return result;
      },
      nb::arg("fluid_properties").none(false),
      nb::arg("rock_properties").none(false), nb::arg("context").none(false),
      nb::arg("p") = nb::none(), nb::arg("T") = nb::none(),
      nb::arg("Sg") = nb::none(), nb::arg("Cag") = nb::none(),
      nb::arg("Cal") = nb::none(), nb::arg("rocktype") = nb::none(),
      R"doc(
Construct a state given a specific context and physical parameters.

Parameters
----------

:param fluid_properties: contains the fluid physical properties necessary for fugacities
:param rock_properties: contains the rock physical properties necessary for the pressure updates
:param context: context (i.e. liquid, gas, diphasic, or specific outflow BC context)
:param p: reference pressure
:param T: temperature
:param Sg: gas phase saturation
:param Cag: gas phase air molar fraction
:param Cal: liquid phase air molar fraction
:param rocktype: rocktype index

)doc");

  module.def(
      "diphasic_equilibrium",
      [](physical_prop::FMP_d &fluid_properties, nb::tuple pressure,
         const double temperature, const double atol,
         const std::size_t maxiter) {
        return diphasic_equilibrium(
            physical_prop::_init_alpha_fugacity_functors(fluid_properties),
            Phase_vector{nb::cast<double>(pressure[0]),
                         nb::cast<double>(pressure[1])},
            temperature, atol, maxiter);
      },
      nb::arg("fluid_properties"), nb::arg("pressure"), nb::arg("temperature"),
      nb::arg("atol") = 1.e-8, nb::arg("maxiter") = 1000);
}
} // namespace physics
