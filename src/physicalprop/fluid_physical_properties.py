import numpy as np
from numba import cfunc, jit, carray, vectorize
from numba.types import void, double, CPointer as p
from compass_utils.units import bar, MPa
from compass_utils import messages
from physicalprop.properties import BothProperties
from physicalprop import cppFluidMixturePropWithDeriv, cppFluidMixtureProperties
from physics.physical_state import PhaseStateStruct, TemperatureStruct


def init_default_fluid_physical_properties(the_physics):
    # creation of the instance init the default physical prop, cpp values
    return FluidMixtureProperties(the_physics)


# contains lists with the phases properties
class FluidMixtureProperties:

    __property_names__ = (
        "dynamic_viscosity",
        "molar_density",
        "volumetric_mass_density",
        "molar_enthalpy",
    )
    psat = None

    def __init__(self, the_physics):
        self.physics = the_physics
        self.phase_state_type = PhaseStateStruct(self.physics.nb_components)
        self.temperature_type = TemperatureStruct()
        # keep python properties
        self.properties = {
            name: [None]
            * self.physics.nb_phases  # list of CompiledBothProperties, one by phase
            for name in self.__property_names__
        }
        # keep cpp equivalents
        self.cpp_prop_with_derivatives = cppFluidMixturePropWithDeriv()
        self.cpp_prop_without_derivatives = cppFluidMixtureProperties()

    def register(self, phase_property_name, phase_id, phase_property):
        self.properties[phase_property_name][phase_id] = CompiledBothProperties(
            self.phase_state_type,
            phase_property.with_derivatives,
            phase_property.without_derivatives,
        )

    # register the phase properties in Python and cpp
    # property_name = 'dynamic_viscosity', 'molar_density', 'mass_density' or 'molar_enthalpy'
    def register_property(
        self,
        property_name,
        property_functions,
        register_c_property_module,
        check_derivatives,
    ):

        n_phases = self.physics.nb_phases
        if n_phases == 1 and not isinstance(property_functions, (list, np.ndarray)):
            #  it is possible that property_functions is not a list
            # if there is only one phase in the physics
            property_functions = [property_functions]
        assert (
            len(property_functions) == n_phases
        ), "You must give one property function per phase"

        for i, prop in enumerate(property_functions):
            self.register(property_name, i, prop)

        if check_derivatives:
            # use the vectorized functions
            self.sanity_check(property_name)

        register_c_property_module.with_derivatives(
            *(p.c_with_derivatives.address for p in self.properties[property_name])
        )
        # necessary if there is a well
        register_c_property_module.without_derivatives(
            *(p.c_without_derivatives.address for p in self.properties[property_name])
        )

    def register_psat(
        self,
        property_function,
        register_c_property_module,
        check_derivatives,
    ):
        self.psat = CompiledTempProperty(
            self.temperature_type,
            property_function.with_derivatives,
            property_function.without_derivatives,
        )
        if check_derivatives:
            # use the vectorized functions
            # self.psat_sanity_check() # todo
            print("psat check derivatives not implemented yet")

        register_c_property_module.with_derivatives(
            self.psat.c_with_derivatives.address
        )
        # necessary if there is a well
        register_c_property_module.without_derivatives(
            self.psat.c_without_derivatives.address
        )

    def __getattr__(self, name):
        if name in self.__property_names__:
            return tuple(self.properties[name])
        raise AttributeError(name)

    def set_components_molar_mass(self, comp_molar_mass):
        comp_mol_mass_with_d = self.cpp_prop_with_derivatives.components_molar_mass
        comp_mol_mass_without_d = (
            self.cpp_prop_without_derivatives.components_molar_mass
        )
        if np.isscalar(comp_molar_mass):
            assert self.physics.nb_components == 1
            # modify cpp values
            comp_mol_mass_with_d[:] = np.array([comp_molar_mass], dtype=float)
            comp_mol_mass_without_d[:] = np.array([comp_molar_mass], dtype=float)
        else:
            # modify cpp values
            comp_mol_mass_with_d[:] = np.asarray(comp_molar_mass)
            comp_mol_mass_without_d[:] = np.asarray(comp_molar_mass)

    def default_linspace_molar_fractions(self, npts):
        if self.physics.nb_components == 1:
            return np.array([np.linspace(0.0, 1.0, npts)]).T
        elif self.physics.nb_components == 2:
            return np.array(
                [np.linspace(0.0, 1.0, npts), 1 - np.linspace(0.0, 1.0, npts)]
            ).T
        else:
            messages.error(
                "No default molar fractions in property_sanity_check when nc > 2"
            )

    def check_phase_property_derivatives(
        self,
        phases_property,
        Xalpha,
        dh=1e-6,
        atol=1e-6,
        rtol=1e-5,
    ):
        ncomp = self.physics.nb_components
        # phases_property contains one property by phase
        for property in phases_property:
            # property contains one function with the derivatives computation
            # and one function without the derivatives computation
            f_without_d = property.without_derivatives  # function val = f(X)
            f_with_d = property.with_derivatives  # function val = f(X, dfdX)

            for fieldname in Xalpha.dtype.names:
                # masks allows to check molar fractions (array) in the same way
                # as pressure and temperature
                masks = [1]
                if fieldname == "molar_fractions":
                    masks = np.identity(ncomp)
                for mask in masks:
                    Xplus = Xalpha.copy()
                    Xplus[fieldname] += dh * mask  # might be out of function definition
                    Xminus = Xalpha.copy()
                    Xminus[fieldname] -= (
                        dh * mask
                    )  # might be out of function definition
                    dfdX = self.phase_state_type.empty_Xalpha(np.size(Xalpha))
                    finite_diff = (f_without_d(Xplus) - f_without_d(Xminus)) / 2 / dh
                    f_with_d(Xalpha, dfdX)  # fill dfdX
                    # extract the good column
                    function_derivatives = dfdX[fieldname].dot(mask)
                    assert np.allclose(
                        finite_diff, function_derivatives, atol=atol, rtol=rtol
                    ), "derivatives are wrong"
        return True

    # check that the value computed with the derivatives val = property(X, dfdX)
    # is equal to the one computed without the derivatives val = property(X)
    def check_phase_property_coherent_values(
        self,
        phases_property,
        Xalpha,
        atol=1e-6,
        rtol=1e-5,
        output_maxerror=False,
    ):
        # phases_property contains one property by phase
        for property in phases_property:
            # property contains one function with derivatives computation
            # and one function without the derivatives computation
            f_with_d = property.with_derivatives  # function val = f(X, dfdX)
            f_without_d = property.without_derivatives  # function val = f(X)
            val_without_d = f_without_d(Xalpha)
            dfdX = self.phase_state_type.empty_Xalpha(np.size(Xalpha))
            val_with_d = f_with_d(Xalpha, dfdX)

            if output_maxerror:
                print(np.fabs(val_with_d - val_without_d).max())
        return np.allclose(val_with_d, val_without_d, atol=atol, rtol=rtol)

    def sanity_check(
        self,
        property_name,
        pressure=np.logspace(np.log10(bar), np.log10(50 * MPa), 100),
        temperature=np.linspace(273.15, 573.15, 100),
        molar_fractions=None,
    ):

        if molar_fractions is None:
            array_size = max(np.size(pressure), np.size(temperature))
            molar_fractions = self.default_linspace_molar_fractions(
                array_size,
            )
        Xalpha = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)

        fluid_mixture_properties = self.properties[property_name]

        # phases_property contains one BothProperties by phase, each phase contains
        # the vectorize property with and without the derivatives
        phases_property = [
            BothProperties(
                with_derivatives=fluid_mixture_properties[i].vect_with_derivatives,
                without_derivatives=fluid_mixture_properties[
                    i
                ].vect_without_derivatives,
            )
            for i in range(self.physics.nb_phases)
        ]

        assert self.check_phase_property_coherent_values(
            phases_property, Xalpha
        ), "Property values differ with and without the derivatives computation"
        assert self.check_phase_property_derivatives(phases_property, Xalpha)

    # common function to compute phase property
    def phase_property(
        self,
        property_name,
        phase_id,
        X,
    ):
        return np.asarray(
            self.properties[property_name][phase_id].vect_without_derivatives(X)
        )

    # brine : salt_molar_fraction is a scalar
    # linear_water : salt_molar_fraction is absent
    def molar_density(self, pressure, temperature, salt_molar_fraction=None):
        iph = self.physics.Phase.single_phase
        molar_fractions = self.physics.build_molar_fractions(salt_molar_fraction)
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)

        return self.phase_property("molar_density", iph, X)

    # diphasic, immiscible2ph : molar_fractions is a vector
    # water2ph : molar_fractions is absent
    def gas_molar_density(self, pressure, temperature, molar_fractions=None):
        iph = self.physics.Phase.gas
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("molar_density", iph, X)

    # diphasic, immiscible2ph : molar_fractions is a vector
    # water2ph : molar_fractions is absent
    def liquid_molar_density(self, pressure, temperature, molar_fractions=None):
        iph = self.physics.Phase.liquid
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("molar_density", iph, X)

    # brine : salt_molar_fraction is a scalar
    # linear_water : salt_molar_fraction is absent
    def volumetric_mass_density(self, pressure, temperature, salt_molar_fraction=None):
        iph = self.physics.Phase.single_phase
        molar_fractions = self.physics.build_molar_fractions(salt_molar_fraction)
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("volumetric_mass_density", iph, X)

    # diphasic, immiscible2ph : molar_fractions is a vector
    # water2ph : molar_fractions is absent
    def gas_volumetric_mass_density(self, pressure, temperature, molar_fractions=None):
        iph = self.physics.Phase.gas
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("volumetric_mass_density", iph, X)

    # diphasic, immiscible2ph : molar_fractions is a vector
    # water2ph : molar_fractions is absent
    def liquid_volumetric_mass_density(
        self, pressure, temperature, molar_fractions=None
    ):
        iph = self.physics.Phase.liquid
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("volumetric_mass_density", iph, X)

    # brine : salt_molar_fraction is a scalar
    # linear_water : salt_molar_fraction is missing
    def dynamic_viscosity(self, pressure, temperature, salt_molar_fraction=None):
        iph = self.physics.Phase.single_phase
        molar_fractions = self.physics.build_molar_fractions(salt_molar_fraction)
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)

        return self.phase_property("dynamic_viscosity", iph, X)

    # diphasic, immiscible2ph : molar_fractions is a vector
    # water2ph : molar_fractions is absent
    def gas_dynamic_viscosity(self, pressure, temperature, molar_fractions=None):
        iph = self.physics.Phase.gas
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("dynamic_viscosity", iph, X)

    # diphasic, immiscible2ph : molar_fractions is a vector
    # water2ph : molar_fractions is absent
    def liquid_dynamic_viscosity(self, pressure, temperature, molar_fractions=None):
        iph = self.physics.Phase.liquid
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("dynamic_viscosity", iph, X)

    # brine : salt_molar_fraction is a scalar
    # linear_water : salt_molar_fraction is missing
    def molar_enthalpy(self, pressure, temperature, salt_molar_fraction=None):
        iph = self.physics.Phase.single_phase
        molar_fractions = self.physics.build_molar_fractions(salt_molar_fraction)
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("molar_enthalpy", iph, X)

    # diphasic, immiscible2ph : molar_fractions is a vector
    # water2ph : molar_fractions is absent
    def gas_molar_enthalpy(self, pressure, temperature, molar_fractions=None):
        iph = self.physics.Phase.gas
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("molar_enthalpy", iph, X)

    # diphasic, immiscible2ph : molar_fractions is a vector
    # water2ph : molar_fractions is absent
    def liquid_molar_enthalpy(self, pressure, temperature, molar_fractions=None):
        iph = self.physics.Phase.liquid
        X = self.phase_state_type.Xalpha(pressure, temperature, molar_fractions)
        return self.phase_property("molar_enthalpy", iph, X)


class CompiledBothProperties:
    def __init__(
        self,
        phase_state_type,
        phase_law_with_derivatives,
        phase_law_without_derivatives,
    ):
        # the functions are stored so the address always exist
        self.phase_state_type = phase_state_type
        self.py_with_derivatives = phase_law_with_derivatives
        self.py_without_derivatives = phase_law_without_derivatives
        # The compilation is done when calling for the first time
        # the compiled function because the python function can be
        # set several times (init with the default one then the user
        # can give an other one)
        self._c_with_derivatives_compiled = None
        self._c_without_derivatives_compiled = None
        self._vect_with_derivatives_compiled = None
        self._vect_without_derivatives_compiled = None

    @property
    def c_with_derivatives(self):
        if self._c_with_derivatives_compiled is None:
            self._c_with_derivatives_compiled = self._compile_c_with_derivatives()
        return self._c_with_derivatives_compiled

    @property
    def c_without_derivatives(self):
        if self._c_without_derivatives_compiled is None:
            self._c_without_derivatives_compiled = self._compile_c_without_derivatives()
        return self._c_without_derivatives_compiled

    @property
    def vect_with_derivatives(self):
        if self._vect_with_derivatives_compiled is None:
            self._vect_with_derivatives_compiled = self._compile_vect_with_derivatives()
        return self._vect_with_derivatives_compiled

    @property
    def vect_without_derivatives(self):
        if self._vect_without_derivatives_compiled is None:
            self._vect_without_derivatives_compiled = (
                self._compile_vect_without_derivatives()
            )
        return self._vect_without_derivatives_compiled

    def _compile_c_without_derivatives(self):
        function_signature = double(p(self.phase_state_type.Xt))  # val = func(Xalpha)
        make_func_cfunc = cfunc(function_signature, nopython=True, cache=True)
        c_f = jit(
            self.py_without_derivatives, nopython=True
        )  # nopython=True by default (soon)

        def c_wrapped_function(X_):
            X = carray(X_, 1)[0]
            return c_f(X)

        return make_func_cfunc(c_wrapped_function)

    def _compile_c_with_derivatives(self):
        subroutine_signature = double(  # val = func(Xalpha, dfdXalpha)
            p(self.phase_state_type.Xt),  # Xalpha
            p(self.phase_state_type.Xt),  # dfdXalpha
        )
        make_sub_cfunc = cfunc(subroutine_signature, nopython=True, cache=True)
        c_f = jit(self.py_with_derivatives, nopython=True)

        def c_wrapped_subroutine(X_, dfdX_):
            X = carray(X_, 1)[0]
            dfdX = carray(dfdX_, 1)[0]
            return c_f(X, dfdX)

        return make_sub_cfunc(c_wrapped_subroutine)

    def _compile_vect_with_derivatives(self):
        return vectorize([double(self.phase_state_type.Xt, self.phase_state_type.Xt)])(
            self.py_with_derivatives
        )

    def _compile_vect_without_derivatives(self):
        return vectorize([double(self.phase_state_type.Xt)])(
            self.py_without_derivatives
        )


class CompiledTempProperty:
    def __init__(
        self,
        temperature_type,
        law_with_derivatives,
        law_without_derivatives,
    ):
        # the functions are stored so the address always exist
        self.temperature_type = temperature_type
        self.py_with_derivatives = law_with_derivatives
        self.py_without_derivatives = law_without_derivatives
        # The compilation is done when calling for the first time
        # the compiled function because the python function can be
        # set several times (init with the default one then the user
        # can give an other one)
        self._c_with_derivatives_compiled = None
        self._c_without_derivatives_compiled = None
        self._vect_with_derivatives_compiled = None
        self._vect_without_derivatives_compiled = None

    @property
    def c_with_derivatives(self):
        if self._c_with_derivatives_compiled is None:
            self._c_with_derivatives_compiled = self._compile_c_with_derivatives()
        return self._c_with_derivatives_compiled

    @property
    def c_without_derivatives(self):
        if self._c_without_derivatives_compiled is None:
            self._c_without_derivatives_compiled = self._compile_c_without_derivatives()
        return self._c_without_derivatives_compiled

    @property
    def vect_with_derivatives(self):
        if self._vect_with_derivatives_compiled is None:
            self._vect_with_derivatives_compiled = self._compile_vect_with_derivatives()
        return self._vect_with_derivatives_compiled

    @property
    def vect_without_derivatives(self):
        if self._vect_without_derivatives_compiled is None:
            self._vect_without_derivatives_compiled = (
                self._compile_vect_without_derivatives()
            )
        return self._vect_without_derivatives_compiled

    def _compile_c_without_derivatives(self):
        function_signature = double(p(self.temperature_type.Xt))  # val = func(Xtemp)
        make_func_cfunc = cfunc(function_signature, nopython=True, cache=True)
        c_f = jit(
            self.py_without_derivatives, nopython=True
        )  # nopython=True by default (soon)

        def c_wrapped_function(X_):
            X = carray(X_, 1)[0]
            return c_f(X)

        return make_func_cfunc(c_wrapped_function)

    def _compile_c_with_derivatives(self):
        subroutine_signature = double(  # val = func(Xtemp, dfdXtemp)
            p(self.temperature_type.Xt),  # Xtemp
            p(self.temperature_type.Xt),  # dfdXtemp
        )
        make_sub_cfunc = cfunc(subroutine_signature, nopython=True, cache=True)
        c_f = jit(self.py_with_derivatives, nopython=True)

        def c_wrapped_subroutine(X_, dfdX_):
            X = carray(X_, 1)[0]
            dfdX = carray(dfdX_, 1)[0]
            return c_f(X, dfdX)

        return make_sub_cfunc(c_wrapped_subroutine)

    def _compile_vect_with_derivatives(self):
        return vectorize([double(self.temperature_type.Xt, self.temperature_type.Xt)])(
            self.py_with_derivatives
        )

    def _compile_vect_without_derivatives(self):
        return vectorize([double(self.temperature_type.Xt)])(
            self.py_without_derivatives
        )
