from physicalprop.set_relative_permeability import (
    set_relative_permeability_functions,
)
from physicalprop.set_capillary_pressure import (
    set_capillary_pressure_functions,
)


# set the relative permeabilities and the capillary pressures,
# more exactly the functions such that
# Palpha = Pref + Pref2Palpha(Xsat)
# with vanGenuchten_kr(Pr, Slr, Sgr, n, Sl_reg=0.99)
def add_van_Genuchten_rock_properties(
    the_rock_properties, rocktype, Pr, Slr, Sgr, n, Sl_reg=0.99
):
    from physicalprop.utils import load_library

    # set the relative permeabilities
    lib_kr = load_library(None, "relative_permeabilities")
    # with vanGenuchten_kr(Pr, Slr, Sgr, n, Sl_reg=0.99)
    set_relative_permeability_functions(
        the_rock_properties, rocktype, lib_kr.vanGenuchten_kr(Pr, Slr, Sgr, n, Sl_reg)
    )

    # set the capillary pressures (pref2palpha)
    lib_pc = load_library(None, "capillary_pressure")
    lib_pc.assert_diphasic_phases_indexes(the_rock_properties.physics)
    set_capillary_pressure_functions(
        the_rock_properties, rocktype, lib_pc.vanGenuchten_pc(Pr, Slr, Sgr, n, Sl_reg)
    )


def set_default_van_Genuchten_rock_properties(the_rock_properties):

    # Cigeo acquifer (on the top of the natural medium)
    add_van_Genuchten_rock_properties(the_rock_properties, 8, 1.5e7, 0, 0, 1.5)
    # Cigeo argilite (natural medium)
    add_van_Genuchten_rock_properties(the_rock_properties, 7, 1.5e7, 0, 0, 1.5)
    # Cigeo EDZ
    add_van_Genuchten_rock_properties(the_rock_properties, 6, 1.5e6, 0, 0, 1.5)
    # Cigeo Bentonite
    add_van_Genuchten_rock_properties(the_rock_properties, 5, 1.6e7, 0, 0, 1.6)
    # Cigeo backfill in access drifts
    add_van_Genuchten_rock_properties(the_rock_properties, 4, 2.0e6, 0, 0, 1.5)
    add_van_Genuchten_rock_properties(the_rock_properties, 3, 2.0e6, 0, 0, 1.5)
    add_van_Genuchten_rock_properties(the_rock_properties, 2, 2.0e6, 0.01, 0, 1.54)
    add_van_Genuchten_rock_properties(the_rock_properties, 1, 15.0e6, 0.4, 0, 1.49)
    add_van_Genuchten_rock_properties(the_rock_properties, 0, 15.0e6, 0.4, 0, 1.49)
