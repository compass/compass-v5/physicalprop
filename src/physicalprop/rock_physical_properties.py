import numpy as np
from numba import cfunc, jit, carray, vectorize
from numba.types import void, double, CPointer as p
from compass_utils.units import bar, MPa
from compass_utils import messages
from physicalprop.properties import BothProperties
from physicalprop import cppRockPropWithDeriv, cppRockProperties
from physics.physical_state import SaturationStruct


def init_default_rock_physical_properties(the_physics):
    # the cpp instanciation is init with default values
    return RockProperties(the_physics)


# contains lists with the rock properties
class RockProperties:

    __property_names__ = (
        "relative_permeability",
        "capillary_pressure",
    )

    def __init__(self, the_physics):
        self.physics = the_physics
        self.saturation_type = SaturationStruct(self.physics.nb_phases)
        # keep python properties
        self.properties = {
            name: {
                # list of CompiledRockProperty, one by rocktype and by phase
                # 0 : [None]
                # * self.physics.nb_phases
            }
            for name in self.__property_names__
        }
        # keep cpp equivalents (dictionary where key is rocktype)
        self.cpp_prop_with_derivatives = cppRockPropWithDeriv()
        self.cpp_prop_without_derivatives = cppRockProperties()

    def register(self, rock_property_name, rocktype_id, phase_id, rock_property):
        self.properties[rock_property_name][rocktype_id][
            phase_id
        ] = CompiledRockProperty(
            self.saturation_type,
            rock_property.with_derivatives,
            rock_property.without_derivatives,
        )

    # register the rock properties in Python and cpp
    # rock_property_name = 'relative_permeability', 'capillary_pressure'
    def register_property(
        self,
        rock_property_name,
        rocktype_id,
        property_functions,
        register_c_property_module,
        check_derivatives,
    ):

        n_phases = self.physics.nb_phases
        if not isinstance(property_functions, (list, np.ndarray)):
            # one property function per phase
            assert n_phases == 1, "Wrong number of properties"
            #  it is possible that property_functions is not a list
            # if there is only one phase in the physics
            property_functions = [property_functions]
        assert len(property_functions) == n_phases, "Wrong number of properties"

        self.properties[rock_property_name].setdefault(
            rocktype_id,
            # list of CompiledRockProperty, one by phase
            [None] * self.physics.nb_phases,
        )
        for i, prop in enumerate(property_functions):
            self.register(rock_property_name, rocktype_id, i, prop)

        if check_derivatives:
            # use the vectorized functions
            self.sanity_check(rock_property_name, rocktype_id)

        register_c_property_module.with_derivatives(
            rocktype_id,
            *(
                p.c_with_derivatives.address
                for p in self.properties[rock_property_name][rocktype_id]
            )
        )
        # necessary if there is a well
        register_c_property_module.without_derivatives(
            rocktype_id,
            *(
                p.c_without_derivatives.address
                for p in self.properties[rock_property_name][rocktype_id]
            )
        )

    def __getattr__(self, name):
        if name in self.__property_names__:
            return tuple(self.properties[name])
        raise AttributeError(name)

    def default_linspace_saturation(self, start, end, npts):
        if self.physics.nb_phases == 1:
            return np.array([np.linspace(start, end, npts)]).T
        elif self.physics.nb_phases == 2:
            return np.array(
                [
                    np.linspace(start, end, npts),
                    1 - np.linspace(start, end, npts),
                ]
            ).T
        else:
            messages.error(
                "No default saturations in property_sanity_check when np > 2"
            )

    def check_rock_property_derivatives(
        self,
        rock_properties,
        Xsat,
        dh=1e-7,
        atol=1e-6,
        rtol=1e-5,
    ):
        nphases = self.physics.nb_phases
        # rock_properties contains one property by phase
        for property in rock_properties:
            # property contains one function with the derivatives computation
            # and one function without the derivatives computation
            f_without_d = property.without_derivatives  # function val = f(X)
            f_with_d = property.with_derivatives  # function val = f(X, dfdX)

            fieldname = "saturation"
            masks = np.identity(nphases)

            for mask in masks:
                Xsat_plus = Xsat.copy()
                # might be out of function definition
                Xsat_plus[fieldname] += dh * mask
                Xsat_minus = Xsat.copy()
                # might be out of function definition
                Xsat_minus[fieldname] -= dh * mask
                dfdX = self.saturation_type.empty_Xsat(np.size(Xsat))
                finite_diff = (
                    (f_without_d(Xsat_plus) - f_without_d(Xsat_minus)) / 2 / dh
                )
                f_with_d(Xsat, dfdX)  # fill dfdX
                # extract the good column
                function_derivatives = dfdX[fieldname].dot(mask)
                assert np.allclose(
                    finite_diff, function_derivatives, atol=atol, rtol=rtol
                ), "derivatives are wrong"
        return True

    # check that the value computed with the derivatives val = property(X, dfdX)
    # is equal to the one computed without the derivatives val = property(X)
    def check_rock_property_coherent_values(
        self,
        rock_properties,
        Xsat,
        atol=1e-6,
        rtol=1e-5,
        output_maxerror=False,
    ):
        # rock_properties contains one property by phase
        for property in rock_properties:
            # property contains one function with derivatives computation
            # and one function without the derivatives computation
            f_with_d = property.with_derivatives  # function val = f(X, dfdX)
            f_without_d = property.without_derivatives  # function val = f(X)
            val_without_d = f_without_d(Xsat)
            dfdX = self.saturation_type.empty_Xsat(np.size(Xsat))
            val_with_d = f_with_d(Xsat, dfdX)

            if output_maxerror:
                print(np.fabs(val_with_d - val_without_d).max())
        return np.allclose(val_with_d, val_without_d, atol=atol, rtol=rtol)

    def sanity_check(
        self,
        rock_property_name,
        rocktype_id,
        saturations=None,
    ):
        if saturations == None:
            saturations = self.default_linspace_saturation(1e-5, 1 - 1e-5, 100)
            if rock_property_name == "capillary_pressure":
                # van genuchten capillary pressure have a certain intervalle of definition
                saturations = self.default_linspace_saturation(1e-5, 0.6 - 1e-4, 100)
        Xsat = self.saturation_type.Xsat(saturations)

        rock_phases_properties = self.properties[rock_property_name][rocktype_id]

        # rock_properties contains one BothProperties by phase, each phase contains
        # the vectorize property with and without the derivatives
        rock_properties = [
            BothProperties(
                with_derivatives=rock_phases_properties[i].vect_with_derivatives,
                without_derivatives=rock_phases_properties[i].vect_without_derivatives,
            )
            for i in range(self.physics.nb_phases)
        ]

        assert self.check_rock_property_coherent_values(
            rock_properties, Xsat
        ), "Property values differ with and without the derivatives computation"
        assert self.check_rock_property_derivatives(rock_properties, Xsat)

    # common function to compute phase property
    # property can be "relative_permeability" or "capillary_pressure"
    def rock_property(
        self,
        rock_property_name,
        phase_id,
        X,
    ):
        return np.asarray(
            self.properties[rock_property_name][phase_id].vect_without_derivatives(X)
        )

    # diphasic, immiscible2ph
    def gas_relative_permeability(self, sat):
        iph = self.physics.Phase.gas
        return self.rock_property("relative_permeability", iph, sat)

    def liquid_relative_permeability(self, sat):
        iph = self.physics.Phase.liquid
        return self.rock_property("relative_permeability", iph, sat)


class CompiledRockProperty:
    def __init__(
        self,
        saturation_type,
        rock_law_with_derivatives,
        rock_law_without_derivatives,
    ):
        # the functions are stored so the address always exist
        self.saturation_type = saturation_type
        self.py_with_derivatives = rock_law_with_derivatives
        self.py_without_derivatives = rock_law_without_derivatives
        # The compilation is done when calling for the first time
        # the compiled function because the python function can be
        # set several times (init with the default one then the user
        # can give an other one)
        self._c_with_derivatives_compiled = None
        self._c_without_derivatives_compiled = None
        self._vect_with_derivatives_compiled = None
        self._vect_without_derivatives_compiled = None

    @property
    def c_with_derivatives(self):
        if self._c_with_derivatives_compiled is None:
            self._c_with_derivatives_compiled = self._compile_c_with_derivatives()
        return self._c_with_derivatives_compiled

    @property
    def c_without_derivatives(self):
        if self._c_without_derivatives_compiled is None:
            self._c_without_derivatives_compiled = self._compile_c_without_derivatives()
        return self._c_without_derivatives_compiled

    @property
    def vect_with_derivatives(self):
        if self._vect_with_derivatives_compiled is None:
            self._vect_with_derivatives_compiled = self._compile_vect_with_derivatives()
        return self._vect_with_derivatives_compiled

    @property
    def vect_without_derivatives(self):
        if self._vect_without_derivatives_compiled is None:
            self._vect_without_derivatives_compiled = (
                self._compile_vect_without_derivatives()
            )
        return self._vect_without_derivatives_compiled

    def _compile_c_without_derivatives(self):
        function_signature = double(p(self.saturation_type.St))  # val = func(X)
        make_func_cfunc = cfunc(function_signature, nopython=True, cache=True)
        c_f = jit(
            self.py_without_derivatives, nopython=True
        )  # nopython=True by default (soon)

        def c_wrapped_function(X_):
            X = carray(X_, 1)[0]
            return c_f(X)

        return make_func_cfunc(c_wrapped_function)

    def _compile_c_with_derivatives(self):
        subroutine_signature = double(  # val = func(X, dfdX)
            p(self.saturation_type.St),  # Xsat
            p(self.saturation_type.St),  # dfdX
        )
        make_sub_cfunc = cfunc(subroutine_signature, nopython=True, cache=True)
        c_f = jit(self.py_with_derivatives, nopython=True)

        def c_wrapped_subroutine(X_, dfdX_):
            X = carray(X_, 1)[0]
            dfdX = carray(dfdX_, 1)[0]
            return c_f(X, dfdX)

        return make_sub_cfunc(c_wrapped_subroutine)

    def _compile_vect_with_derivatives(self):
        return vectorize([double(self.saturation_type.St, self.saturation_type.St)])(
            self.py_with_derivatives
        )

    def _compile_vect_without_derivatives(self):
        return vectorize([double(self.saturation_type.St)])(self.py_without_derivatives)
