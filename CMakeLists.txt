cmake_minimum_required(VERSION 3.24)
cmake_policy(VERSION 3.24)

project(physicalprop LANGUAGES CXX)

find_package(compass)

compass_configure_build()

add_subdirectory(src)

compass_export_project_targets()
