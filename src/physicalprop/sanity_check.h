#pragma once

#include <cassert>
#include <cmath>
#include <physicalprop/physicalprop.h>
#include <physics/model.h>

namespace physical_prop {

// this file compiles only with diphasic physics
inline void test_default_psat(FMP &fluid_properties_no_d,
                              FMP_d &fluid_properties) {
  double eps = 1.e-8;
  auto X = Xtemp{350.};
  auto psat = fluid_properties_no_d.psat()(X);
  auto psat_res =
      100.0 * exp(46.784 - 6435.0 / X.temperature - 3.868 * log(X.temperature));
  assert(fabs(psat - psat_res) < eps);
  auto &&[psat_with_d, derivative] = fluid_properties.psat()(X);
  auto derivatire_res =
      (6435.0 / std::pow(X.temperature, 2.0) - 3.868 / X.temperature) *
      psat_res;
  assert(fabs(psat_with_d - psat_res) < eps);
  assert(fabs(derivative.temperature - derivatire_res) < eps);
}

// this file compiles only with diphasic physics
inline void test_visco_no_derivatives(FMP &fluid_properties, double val_g,
                                      double val_l) {
  double eps = 1.e-8;
  auto pg = 1.e5;
  auto pl = pg;
  auto temp = 175.0 + 25.0;
  auto X0 =
      Xalpha{.pressure = pg, .temperature = temp, .molar_fractions = {1, 0}};
  auto visco_g = fluid_properties.viscosity(Phase::gas)(X0);
  assert(fabs(visco_g - val_g) < eps);
  auto X1 =
      Xalpha{.pressure = pl, .temperature = temp, .molar_fractions = {0, 1}};
  auto visco_l = fluid_properties.viscosity(Phase::liquid)(X1);
  assert(fabs(visco_l - val_l) < eps);
}
inline void test_visco_with_derivatives(FMP_d &fluid_properties, double val_g,
                                        double val_l) {
  double eps = 1.e-8;
  auto pg = 1.e5;
  auto pl = pg;
  auto temp = 175.0 + 25.0;
  auto X0 =
      Xalpha{.pressure = pg, .temperature = temp, .molar_fractions = {1, 0}};
  auto &&[visco_g, ddXg] = fluid_properties.viscosity(Phase::gas)(X0);
  auto X1 =
      Xalpha{.pressure = pl, .temperature = temp, .molar_fractions = {0, 1}};
  auto &&[visco_l, ddXl] = fluid_properties.viscosity(Phase::liquid)(X1);
  assert(fabs(visco_g - val_g) < eps);
  assert(fabs(visco_l - val_l) < eps);
  for (auto &&ddX : {ddXg, ddXl}) {
    assert(fabs(ddX.temperature) < eps);
    assert(fabs(ddX.pressure) < eps);
    for (size_t comp = 0; comp < nb_components; ++comp) {
      assert(fabs(ddX.molar_fractions[comp]) < eps);
    }
  }
  // test with State instead of Xalpha
  auto var_visco_g =
      make_variable<Phase::gas>(fluid_properties.viscosity(Phase::gas));
  auto XS0 = State{};
  XS0.saturation[Phase::gas] = 1.0;
  XS0.temperature = temp;
  for (auto alpha : all_phases::array)
    XS0.pressure[alpha] = pg;
  XS0.molar_fractions[Phase::gas] = Component_vector{0.9, 0.1};
  XS0.molar_fractions[Phase::liquid] = Component_vector{0.2, 0.8};
  auto &&[visco_g_S0, ddXgS0] = var_visco_g(XS0);
  assert(fabs(visco_g_S0 - val_g) < eps);
  assert(fabs(ddXgS0.temperature) < eps);
  for (auto alpha : all_phases::array) {
    assert(fabs(ddXgS0.pressure[alpha]) < eps);
    assert(fabs(ddXgS0.saturation[alpha]) < eps);
    for (auto comp : all_components::array) {
      assert(fabs(ddXgS0.molar_fractions[alpha][comp]) < eps);
    }
  }
}

inline void test_fugacity(FMP &fluid_properties_no_d, FMP_d &fluid_properties) {
  double eps = 1.e-8;
  auto pg = 1.e5;
  auto temp = 175.0 + 25.0;
  auto Ca = 0.6;
  auto Cw = 1. - Ca;
  auto X0 = Xalpha{.pressure = pg, .temperature = temp};
  X0.molar_fractions[Component::air] = Ca;
  X0.molar_fractions[Component::water] = Cw;
  auto air_index = component_index(Component::air);
  auto water_index = component_index(Component::water);

  // without derivatives
  auto &&gas_fug_no_d =
      physics::f_fugacity_coefficients_no_d<Phase::gas>(fluid_properties_no_d);
  auto &&gas_fug_coef_no_d = gas_fug_no_d(X0);
  auto &&liq_fug_no_d = physics::f_fugacity_coefficients_no_d<Phase::liquid>(
      fluid_properties_no_d);
  auto &&liq_fug_coef_no_d = liq_fug_no_d(X0);

  // with derivatives
  // gas
  auto &&gas_fug = _fugacity_coefficients<Phase::gas>(fluid_properties);
  auto &&gas_fug_coef = gas_fug(X0);
  for (auto &&[comp_fug, comp_fug_no_d] :
       std::views::zip(gas_fug_coef.value, gas_fug_coef_no_d)) {
    assert(fabs(comp_fug - pg) < eps);
    assert(fabs(comp_fug - comp_fug_no_d) < eps);
  }
  for (auto &&ddX : gas_fug_coef.derivatives) {
    assert(fabs(ddX.temperature) < eps);
    assert(fabs(ddX.pressure - 1.) < eps);
    for (auto &&comp : all_components::array)
      assert(fabs(ddX.molar_fractions[comp]) < eps);
    // liquid
    auto &&liq_fug = _fugacity_coefficients<Phase::liquid>(fluid_properties);
    auto &&liq_fug_coef = liq_fug(X0);
    for (auto &&[comp_fug, comp_fug_no_d] :
         std::views::zip(liq_fug_coef.value, liq_fug_coef_no_d))
      assert(fabs(comp_fug - comp_fug_no_d) < eps);
    // air comp
    double ddHT = (10.e9 - 6.e9) / (353. - 293.);
    double H = 6.e9 + (temp - 293.) * ddHT;
    double fla = H;
    assert(fabs((liq_fug_coef.value[air_index] - fla) / fla) <
           eps); // scale norm
    assert(fabs(liq_fug_coef.derivatives[air_index].pressure) < eps);
    assert(fabs(liq_fug_coef.derivatives[air_index].temperature - ddHT) < eps);
    for (auto &&comp : all_components::array)
      assert(fabs(liq_fug_coef.derivatives[air_index].molar_fractions[comp]) <
             eps);
  }

  // fugacity
  auto Xstate =
      State{.pressure = {X0.pressure, X0.pressure},
            .temperature = X0.temperature,
            .molar_fractions = {X0.molar_fractions, X0.molar_fractions}};
  auto &&fg_no_d = _fugacity<Phase::gas>(fluid_properties_no_d);
  auto &&fug_g_no_d = fg_no_d(Xstate);
  auto &&fg = _fugacity<Phase::gas>(fluid_properties);
  auto &&fug_g = fg(X0);
  for (auto &&comp : all_components::array)
    assert(fabs(fug_g_no_d[comp] - fug_g.value[component_index(comp)]) < eps);
  assert(fabs(fug_g.value[air_index] - gas_fug_coef.value[air_index] * Ca) <
         eps);
  assert(fabs(fug_g.value[water_index] - gas_fug_coef.value[water_index] * Cw) <
         eps);
  auto &&fl_no_d = _fugacity<Phase::liquid>(fluid_properties_no_d);
  auto &&fug_l_no_d = fl_no_d(Xstate);
  auto &&fl = _fugacity<Phase::liquid>(fluid_properties);
  auto &&fug_l = fl(X0);
  for (auto &&comp : all_components::array)
    assert(fabs(fug_l_no_d[comp] - fug_l.value[component_index(comp)]) < eps);
  // fugacity check done in coats-variable
}

inline void test_rock_heat_capacity(RP &rock_properties_no_d,
                                    RP_d &rock_properties_with_d) {

  double eps = 1.e-8;
  assert(fabs(rock_properties_no_d.rock_heat_capacity - 1.6e6) < eps);
  assert(fabs(rock_properties_with_d.rock_heat_capacity - 1.6e6) < eps);
}

inline void test_rel_perm_no_derivatives(int rocktype, RP &rock_properties) {
  double eps = 1.e-8;
  auto phase_relative_perm =
      phase_functors(rock_properties.relative_perm(rocktype, Phase::gas),
                     rock_properties.relative_perm(rocktype, Phase::liquid));
  for (auto alpha : all_phases::array) {
    auto alpha_i = phase_index(alpha);
    Phase_vector sat{};
    sat[alpha_i] = 1;
    auto Xsat_alpha = Xsat{.saturation = sat};
    auto &&rel_perm_alpha = phase_relative_perm.apply(alpha, Xsat_alpha);
    assert(fabs(rel_perm_alpha - 1) < eps);
    auto sum_rel_perm = sum_on_present_phases(Context::diphasic,
                                              phase_relative_perm, Xsat_alpha);
    assert(fabs(sum_rel_perm - 1) < eps);
  }
}
inline void test_rel_perm_with_derivatives(int rocktype,
                                           RP_d &rock_properties) {
  double eps = 1.e-8;
  auto phase_relative_perm =
      phase_functors(rock_properties.relative_perm(rocktype, Phase::gas),
                     rock_properties.relative_perm(rocktype, Phase::liquid));
  for (auto alpha : all_phases::array) {
    auto alpha_i = phase_index(alpha);
    Phase_vector sat{};
    sat[alpha_i] = 1;
    auto Xsat_alpha = Xsat{.saturation = sat};
    auto &&[rel_perm, ddXrel_perm] =
        phase_relative_perm.apply(alpha, Xsat_alpha);
    assert(fabs(rel_perm - 1) < eps);
    assert(fabs(ddXrel_perm.saturation[alpha_i] - 2) < eps);
    auto &&[sum, ddXsum] = sum_on_present_phases(
        Context::diphasic, phase_relative_perm, Xsat_alpha);
    assert(fabs(sum - 1) < eps);
    assert(fabs(ddXsum.saturation[alpha_i] - 2) < eps);
  }
  // test with Sg = Sl = 0.5
  Phase_vector sat{0.5, 0.5};
  auto Xsat_alpha = Xsat{sat};
  auto &&[rel_perm, ddXrel_perm] =
      phase_relative_perm.apply(Phase::liquid, Xsat_alpha);
  assert(fabs(rel_perm - 0.25) < eps);
  assert(fabs(ddXrel_perm.saturation[Phase::liquid] - 1) < eps);
  assert(fabs(ddXrel_perm.saturation[Phase::gas]) < eps);
  auto &&[sum, ddXsum] =
      sum_on_present_phases(Context::diphasic, phase_relative_perm, Xsat_alpha);
  assert(fabs(sum - 0.5) < eps);
  assert(fabs(ddXsum.saturation[Phase::liquid] - 1) < eps);
  assert(fabs(ddXsum.saturation[Phase::gas] - 1) < eps);

  // test with State instead of Xsat
  auto rel_perm_l =
      make_sat_variable(rock_properties.relative_perm(rocktype, Phase::liquid));
  auto XS0 = State{};
  XS0.saturation[Phase::gas] = 1.0;
  XS0.temperature = 280;
  for (auto alpha : all_phases::array)
    XS0.pressure[alpha] = 1e5;
  XS0.molar_fractions[Phase::gas] = Component_vector{0.9, 0.1};
  XS0.molar_fractions[Phase::liquid] = Component_vector{0.2, 0.8};
  auto &&[rel_perm_S0, ddXgS0] = rel_perm_l(XS0);
  assert(fabs(rel_perm_S0) < eps);
  assert(fabs(ddXgS0.temperature) < eps);
  for (auto alpha : all_phases::array) {
    assert(fabs(ddXgS0.pressure[alpha]) < eps);
    assert(fabs(ddXgS0.saturation[alpha]) < eps);
    for (auto comp : all_components::array) {
      assert(fabs(ddXgS0.molar_fractions[alpha][comp]) < eps);
    }
  }
}

inline void test_van_genuchten_kr_with_derivatives(int rocktype,
                                                   RP_d &rock_properties,
                                                   Real Slr, Real Sgr) {
  double eps = 1.e-8;
  double Sl_reg = 0.99;
  auto phase_relative_perm =
      phase_functors(rock_properties.relative_perm(rocktype, Phase::gas),
                     rock_properties.relative_perm(rocktype, Phase::liquid));
  Xsat Xs;
  // gas : Sg >= 1 - Slr
  constexpr int n_pts = 20;
  auto dh = Slr / n_pts;
  for (int i = 0; i < n_pts; ++i) {
    Xs.saturation[Phase::gas] = (1 - Slr) + i * dh;
    Xs.saturation[Phase::liquid] = 1 - Xs.saturation[Phase::gas];
    auto &&[rel_perm_g, ddXrel_perm_g] =
        phase_relative_perm.apply(Phase::gas, Xs);
    assert(fabs(rel_perm_g - 1) < eps);
    auto &&[sum, ddXsum] =
        sum_on_present_phases(Context::diphasic, phase_relative_perm, Xs);
    assert(fabs(sum - 1) < eps);
    // skip derivatives assert, ddX_liq is non nul
    if (Xs.saturation[Phase::liquid] <= 1 - Sl_reg)
      continue;
    for (auto alpha : all_phases::array) {
      assert(fabs(ddXrel_perm_g.saturation[alpha]) < eps);
      assert(fabs(ddXsum.saturation[alpha]) < eps);
    }
  }
}

inline void test_van_genuchten_pc_with_derivatives(int rocktype,
                                                   RP_d &rock_properties,
                                                   Real Slr, Real Sgr) {
  double eps = 1.e-8;

  auto phase_pref2palpha =
      phase_functors(rock_properties.pref2palpha(rocktype, Phase::gas),
                     rock_properties.pref2palpha(rocktype, Phase::liquid));
  Xsat Xs; // no init needed, not used in default pc (= 0)
  auto &&pref2pgas = phase_pref2palpha.apply(Phase::gas, Xs);
  assert(fabs(pref2pgas.value) < eps);
  for (auto alpha : all_phases::array)
    assert(fabs(pref2pgas.derivatives.saturation[alpha]) < eps);
}

inline void test_default_pc(RP &rock_properties_no_d,
                            RP_d &rock_properties_with_d) {
  double eps = 1.e-8;
  int rocktype = 0;
  auto phase_pref2palpha = phase_functors(
      rock_properties_with_d.pref2palpha(rocktype, Phase::gas),
      rock_properties_with_d.pref2palpha(rocktype, Phase::liquid));
  Xsat Xs; // no init needed, not used in default pc (= 0)
  auto &&[val, ddX] =
      sum_on_present_phases(Context::diphasic, phase_pref2palpha, Xs);
  assert(fabs(val) < eps);
  for (auto alpha : all_phases::array)
    assert(fabs(ddX.saturation[alpha]) < eps);

  // no derivatives
  auto phase_pref2palpha_no_d =
      phase_functors(rock_properties_no_d.pref2palpha(rocktype, Phase::gas),
                     rock_properties_no_d.pref2palpha(rocktype, Phase::liquid));
  assert(fabs(sum_on_present_phases(Context::diphasic, phase_pref2palpha_no_d,
                                    Xs)) < eps);
}
} // namespace physical_prop
