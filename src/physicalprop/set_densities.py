from numba import jit
import numpy as np

from compass_utils import messages
from physicalprop.properties import BothProperties


def set_molar_density_functions(
    physical_prop,
    functions,
    *,
    check_derivatives=True,
    _update_volumetric_mass_density_functions=True,
):
    """
    Set the molar density functions (with and without the derivatives)
    for all phases.
    :param functions: list of BothProperties (one by phase)
    :param check_derivatives: (True by default) check the derivatives of the property
    :param _update_volumetric_mass_density_functions: (True by default) update automatically the volumetric mass density functions
    """
    if _update_volumetric_mass_density_functions:
        messages.warning(
            "overriding default molar and volumetric mass density functions"
        )
    else:
        messages.warning(
            "overriding default molar density functions, no update of mass densities"
        )

    register_c_property_module = BothProperties(
        with_derivatives=physical_prop.cpp_prop_with_derivatives.register_c_molar_densities_with_derivatives,
        without_derivatives=physical_prop.cpp_prop_without_derivatives.register_c_molar_densities_without_derivatives,
    )
    physical_prop.register_property(
        "molar_density",
        functions,
        register_c_property_module,
        check_derivatives,
    )
    if _update_volumetric_mass_density_functions:
        _set_volumetric_mass_density_functions(
            physical_prop,
            check_derivatives=check_derivatives,
        )


def _set_volumetric_mass_density_functions(
    physical_prop,
    *,
    check_derivatives=True,
):
    """
    Set the volumetric mass density functions (with and without the derivatives)
    for all phases with volumetric_mass_density = (Sum Ci Mi)*molar_density
    :param check_derivatives: (True by default) check the derivatives of the property
    """
    functions = build_volumetric_mass_density_from_molar_density(
        physical_prop,
    )

    register_c_property_module = BothProperties(
        with_derivatives=physical_prop.cpp_prop_with_derivatives.register_c_volumetric_mass_densities_with_derivatives,
        without_derivatives=physical_prop.cpp_prop_without_derivatives.register_c_volumetric_mass_densities_without_derivatives,
    )
    physical_prop.register_property(
        "volumetric_mass_density",
        functions,
        register_c_property_module,
        check_derivatives,
    )


def set_components_molar_mass(
    physical_prop,
    comp_molar_mass,
):
    if physical_prop.properties["molar_density"][0] is None:
        messages.error(
            "you must set_molar_density_functions before set_components_molar_mass"
        )
    messages.warning(
        "overriding default components molar mass and mass density functions"
    )

    physical_prop.set_components_molar_mass(comp_molar_mass)
    _set_volumetric_mass_density_functions(physical_prop)


def build_volumetric_mass_density_from_molar_density(
    physical_prop,
):
    assert (
        physical_prop.cpp_prop_with_derivatives.components_molar_mass
        == physical_prop.cpp_prop_without_derivatives.components_molar_mass
    ).all()
    # np.array of size n_comp
    comp_molar_mass = np.copy(
        physical_prop.cpp_prop_with_derivatives.components_molar_mass
    )
    volumetric_mass_density_functions = []
    for phase_property in physical_prop.properties["molar_density"]:
        volumetric_mass_density_functions.append(
            build_phase_volumetric_mass_density_from_molar_density(
                comp_molar_mass,
                BothProperties(
                    with_derivatives=phase_property.py_with_derivatives,
                    without_derivatives=phase_property.py_without_derivatives,
                ),
            )
        )

    return volumetric_mass_density_functions


def build_phase_volumetric_mass_density_from_molar_density(
    comp_molar_mass,  # np.array of size n_comp
    phase_molar_density_functions,  # BothProperties
):
    # is it necessary to compile molar_density first ?
    compiled_phase_molar_densities_with_derivatives = jit(
        phase_molar_density_functions.with_derivatives, nopython=True
    )
    compiled_phase_molar_densities_without_derivatives = jit(
        phase_molar_density_functions.without_derivatives, nopython=True
    )

    def volumetric_mass_density_with_derivatives(X, dfdX):
        phase_molar_density = compiled_phase_molar_densities_with_derivatives(X, dfdX)
        # I removed MCP from the formula
        # MCP(AIR_COMP, iph)*M_air*C(AIR_COMP) + MCP(WATER_COMP, iph)*M_H2O*C(WATER_COMP)
        weighted_components_molar_mass = X.molar_fractions.dot(comp_molar_mass)
        dfdX.pressure *= weighted_components_molar_mass
        dfdX.temperature *= weighted_components_molar_mass
        # is isscalar quicker than empty list + append + conversion to np.array ?
        if np.isscalar(phase_molar_density):
            weighted_phase_molar_density = phase_molar_density * comp_molar_mass
        else:
            weighted_phase_molar_density = []
            for Mcomp in comp_molar_mass:
                weighted_phase_molar_density.append(Mcomp * phase_molar_density)
            weighted_phase_molar_density = np.array(weighted_phase_molar_density).T
        dfdX.molar_fractions *= weighted_components_molar_mass
        dfdX.molar_fractions += weighted_phase_molar_density
        return weighted_components_molar_mass * phase_molar_density

    def volumetric_mass_density_without_derivatives(X):
        phase_molar_density = compiled_phase_molar_densities_without_derivatives(X)
        # I removed MCP from the formula
        # MCP(AIR_COMP, iph)*M_air*C(AIR_COMP) + MCP(WATER_COMP, iph)*M_H2O*C(WATER_COMP)
        weighted_components_molar_mass = X.molar_fractions.dot(comp_molar_mass)
        return weighted_components_molar_mass * phase_molar_density

    return BothProperties(
        with_derivatives=volumetric_mass_density_with_derivatives,
        without_derivatives=volumetric_mass_density_without_derivatives,
    )


def assert_diphasic_phase_indexes(the_physics):
    # assumed in diphasic viscosities definition
    assert the_physics.Phase.gas == 0
    assert the_physics.Phase.liquid == 1
