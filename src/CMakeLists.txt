compass_add_dependencies(PACKAGES pscal physics)

compass_header_only_library(physicalprop FILES physicalprop.h INTERFACE pscal
                            diphasic-physics)

compass_add_nb_module(bindings FILES bindings.cpp PRIVATE physicalprop)
