def set_rock_volumetric_heat_capacity(
    rock_physical_prop,
    rock_heat_capacity=1.6e6,  # by default 800.0*2000.0 J/m3
):
    rock_physical_prop.cpp_prop_with_derivatives.rock_heat_capacity = rock_heat_capacity
    rock_physical_prop.cpp_prop_without_derivatives.rock_heat_capacity = (
        rock_heat_capacity
    )
