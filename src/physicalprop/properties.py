from types import FunctionType
from typing import NamedTuple
import numpy as np
from physicalprop import build_state


class BothProperties(NamedTuple):
    with_derivatives: FunctionType
    without_derivatives: FunctionType


class Properties:
    """Properties struct with fluid and rock prop, build_state..."""

    def __init__(
        self,
        fluid_properties,
        rock_properties,
    ):
        self.fluid = fluid_properties
        self.rock = rock_properties

    def build_state(self, context, *, p, T, Sg=None, Cal=None, Cag=None, rocktype=None):
        state = build_state(
            self.fluid.cpp_prop_with_derivatives,
            self.rock.cpp_prop_with_derivatives,
            context,
            p=p,
            T=T,
            Sg=Sg,
            Cal=Cal,
            Cag=Cag,
            rocktype=rocktype,
        )
        return context, state
