#pragma once
#include "physics/model.h"
#include "pscal/value.h"
#include "pscal/variable.h"

namespace physical_prop {

using namespace physics;

template <Phase alpha, typename Func> inline auto make_variable(Func func) {
  return pscal::Variable<State2Xalpha<alpha>>(func);
}
template <typename Func> inline auto make_sat_variable(Func func) {
  return pscal::Variable<State2Xsat>(func);
}
template <typename Func> inline auto make_temp_variable(Func func) {
  return pscal::Variable<State2Xtemp>(func);
}
template <typename Func> inline auto make_variable_IdState(Func func) {
  return pscal::Variable<pscal::IdMap<State>>(func);
}
template <Phase alpha, typename Func>
inline auto make_vect_variable(Func func) {
  return pscal::Variable<State2Xalpha<alpha>, nb_components>(func);
}
template <typename Func> inline auto make_vect_variable_IdState(Func func) {
  return pscal::Variable<pscal::IdMap<State>, nb_components>(func);
}

inline auto convert_api(const Phase_property_with_derivatives_ptr func) {
  return [func](const Xalpha &x) {
    Xalpha ddX;
    Real val = func(x, ddX);
    return pscal::Scalar<Xalpha>(val, ddX);
  };
}
inline auto convert_api(const Rock_property_with_derivatives_ptr func) {
  return [func](const Xsat &x) {
    Xsat ddX;
    Real val = func(x, ddX);
    return pscal::Scalar<Xsat>(val, ddX);
  };
}
inline auto convert_api(const Temp_property_with_derivatives_ptr func) {
  return [func](const Xtemp &x) {
    Xtemp ddX;
    Real val = func(x, ddX);
    return pscal::Scalar<Xtemp>(val, ddX);
  };
}
inline auto convert_api(const Phase_property_ptr func) { return func; }
inline auto convert_api(const Rock_property_ptr func) { return func; }
inline auto convert_api(const Temp_property_ptr func) { return func; }

// one instance by rocktype
template <typename prop_ptr> struct RockProperties {
  using Rocktype_phase_property = RocktypePhaseProperty<prop_ptr>;
  std::vector<int> _rel_perm_rocktypes;
  std::vector<int> _pc_rocktypes;
  Rocktype_phase_property _relative_permeabilities;
  Rocktype_phase_property _pref2palpha;
  Real rock_heat_capacity;

  // constructor with default physical prop, can be modified from python
  RockProperties() {
    if constexpr (std::is_same<prop_ptr,
                               Rock_property_with_derivatives_ptr>::value) {
      _relative_permeabilities =
          default_rel_perm_with_derivatives(_rel_perm_rocktypes);
      _pref2palpha = default_pref2palpha_with_derivatives(_pc_rocktypes);
    } else if constexpr (std::is_same<prop_ptr, Rock_property_ptr>::value) {
      _relative_permeabilities =
          default_rel_perm_without_derivatives(_rel_perm_rocktypes);
      _pref2palpha = default_pref2palpha_without_derivatives(_pc_rocktypes);
    }
    rock_heat_capacity = default_rock_volumetric_heat_capacity();
  }

  auto relative_perm(const int rocktype, const Phase alpha) {
    return convert_api(_relative_permeabilities[rocktype][alpha]);
  }
  auto pref2palpha(const int rocktype, const Phase alpha) {
    return convert_api(_pref2palpha[rocktype][alpha]);
  }
};
using RP_d = RockProperties<Rock_property_with_derivatives_ptr>;
using RP = RockProperties<Rock_property_ptr>;

template <typename prop_ptr, typename T_prop> struct FluidMixtureProperties {
  using Phase_property = PhaseProperty<prop_ptr>;
  Phase_property _dynamic_viscosities;
  Phase_property _molar_densities;
  Component_vector components_molar_mass;
  Phase_property _volumetric_mass_densities;
  Phase_property _molar_enthalpies;
  T_prop _psat;

  // constructor with default physical prop, can be modified from python
  // template<std::enable_if_t<std::is_same<prop_ptr,
  // Phase_property_with_derivatives_ptr>::value, int>* = nullptr>
  FluidMixtureProperties() {
    if constexpr (std::is_same<prop_ptr,
                               Phase_property_with_derivatives_ptr>::value) {
      _dynamic_viscosities = default_viscosity_with_derivatives();
      _molar_densities = default_molar_density_with_derivatives();
      // init default density and molar mass
      _volumetric_mass_densities =
          default_volumetric_mass_density_with_derivatives(
              components_molar_mass);
      _molar_enthalpies = default_molar_enthalpy_with_derivatives();
    } else if constexpr (std::is_same<prop_ptr, Phase_property_ptr>::value) {
      _dynamic_viscosities = default_viscosity_without_derivatives();
      _molar_densities = default_molar_density_without_derivatives();
      // init default density and molar mass
      _volumetric_mass_densities =
          default_volumetric_mass_density_without_derivatives(
              components_molar_mass);
      _molar_enthalpies = default_molar_enthalpy_without_derivatives();
    }
    if constexpr (std::is_same<T_prop,
                               Temp_property_with_derivatives_ptr>::value) {
      _psat = default_psat_with_derivatives();
    } else if constexpr (std::is_same<T_prop, Temp_property_ptr>::value) {
      _psat = default_psat_without_derivatives();
    }
  }

  auto viscosity(const Phase alpha) {
    return convert_api(_dynamic_viscosities[alpha]);
  }
  auto molar_density(Phase alpha) {
    return convert_api(_molar_densities[alpha]);
  }
  auto density(Phase alpha) {
    return convert_api(_volumetric_mass_densities[alpha]);
  }
  auto molar_enthalpy(Phase alpha) {
    return convert_api(_molar_enthalpies[alpha]);
  }
  auto psat() { return convert_api(_psat); }
};
using FMP_d = FluidMixtureProperties<Phase_property_with_derivatives_ptr,
                                     Temp_property_with_derivatives_ptr>;
using FMP = FluidMixtureProperties<Phase_property_ptr, Temp_property_ptr>;

// convert fug coef into pscal struct when there are the derivatives
// func is evaluated with Xalpha, pscal will do the conversion with State
template <physics::Phase ph>
inline auto _fugacity_coefficients(FMP_d &fluid_properties) {
  auto &&phase_fug_coeff = f_fugacity_coefficients<ph>(fluid_properties);
  return [phase_fug_coeff](const Xalpha &X) {
    pscal::Vector<Xalpha, nb_components> res{};
    res.value = phase_fug_coeff(X, res.derivatives);
    return res;
  };
}

// init _fugacity = fug_coef * Ci with derivatives
template <physics::Phase ph> inline auto _fugacity(FMP_d &fluid_properties) {
  // using compass::std_ranges::views::zip;
  using namespace compass::utils::array_operators;
  auto &&phase_fug_coeff = _fugacity_coefficients<ph>(fluid_properties);
  return [phase_fug_coeff](const Xalpha &X) {
    auto res = phase_fug_coeff(X);
    auto comp = 0;
    for (auto &&resd : res.derivatives) {
      resd.pressure *= X.molar_fractions[comp];
      resd.temperature *= X.molar_fractions[comp];
      resd.molar_fractions *= X.molar_fractions[comp];
      resd.molar_fractions[comp] += res.value[comp]; // value = fug coef
      ++comp;
    }
    res.value *= X.molar_fractions;

    return res;
  };
}

template <physics::Phase... ph>
inline auto _alpha_fugacity_functors(Phase_set<ph...>,
                                     FMP_d &fluid_properties) {
  return phase_functors(_fugacity<ph>(fluid_properties)...);
}
inline auto _init_alpha_fugacity_functors(FMP_d &fluid_properties) {
  return _alpha_fugacity_functors(all_phases{}, fluid_properties);
}

// convert fug coef to be called with State when there are no derivative
// pscal will not be used, need to define a function called over State
template <physics::Phase ph>
inline auto _fugacity_coefficients(FMP &fluid_properties) {
  auto &&phase_fug_coeff = f_fugacity_coefficients_no_d<ph>(fluid_properties);
  return [phase_fug_coeff](const State &X) {
    auto Xa = Xalpha{.pressure = X.pressure[ph],
                     .temperature = X.temperature,
                     .molar_fractions = X.molar_fractions[ph]};
    return phase_fug_coeff(Xa);
  };
}

// init _fugacity = fug_coef * Ci without derivatives
template <physics::Phase ph> inline auto _fugacity(FMP &fluid_properties) {
  using namespace compass::utils::array_operators;
  auto &&phase_fug_coeff = _fugacity_coefficients<ph>(fluid_properties);
  return [phase_fug_coeff](const State &X) {
    auto &&res = phase_fug_coeff(X);
    res *= X.molar_fractions[ph];
    return res;
  };
}

} // namespace physical_prop
