from compass_utils import messages
from physicalprop.utils import load_library
from physicalprop.properties import BothProperties


def set_viscosity_functions(
    physical_prop,
    functions=None,
    *,
    check_derivatives=True,
    library=None,
):
    """
    Set the viscosity functions (with and without the derivatives)
    for all phases.
    If no viscosity function is given, use default values.
    To use another function, give the function
    :param functions: (optional) list of BothProperties (one by phase)
    :param check_derivatives: (True by default) check the derivatives of the property
    """
    if functions is not None:
        messages.warning("overriding default viscosity functions")
    else:
        functions = get_default_viscosity_functions(physical_prop.physics, library)

    register_c_property_module = BothProperties(
        with_derivatives=physical_prop.cpp_prop_with_derivatives.register_c_viscosities_with_derivatives,
        without_derivatives=physical_prop.cpp_prop_without_derivatives.register_c_viscosities_without_derivatives,
    )
    physical_prop.register_property(
        "dynamic_viscosity",
        functions,
        register_c_property_module,
        check_derivatives,
    )


def get_default_viscosity_functions(the_physics, library):
    lib = load_library(library, "viscosities")
    physics_name = the_physics.name()

    if physics_name == "diphasic" or physics_name == "immiscible2ph":
        assert_diphasic_phase_indexes(the_physics)
        return [
            lib.gas_diphasic_viscosities,
            lib.liquid_diphasic_viscosities,
        ]

    elif physics_name == "water2ph":
        assert_diphasic_phase_indexes(the_physics)
        return [lib.gas_water2ph_viscosities, lib.liquid_water2ph_viscosities]

    elif physics_name == "linear_water":
        return lib.pure_phase_viscosities

    elif physics_name == "brine":
        lib.assert_salt_component_index(the_physics)
        return lib.brine_viscosities

    else:
        raise "viscosity not implemented for this physics"


def assert_diphasic_phase_indexes(the_physics):
    # assumed in diphasic viscosities definition
    assert the_physics.Phase.gas == 0
    assert the_physics.Phase.liquid == 1
