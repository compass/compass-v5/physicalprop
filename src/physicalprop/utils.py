from importlib import import_module
import numpy as np
from physicalprop.properties import BothProperties

DEFAULT_LIBRARY = "prop_library"


def constant_fluid_physical_property(a):
    def cst_property_with_derivatives(X, dfdX):
        dfdX.pressure = 0
        dfdX.temperature = 0
        dfdX.molar_fractions.fill(0)
        return a  # necessary to init with np.double(a) ?

    def cst_property_without_derivatives(X):
        return a  # necessary to init with np.double(a) ?

    return BothProperties(
        with_derivatives=cst_property_with_derivatives,
        without_derivatives=cst_property_without_derivatives,
    )


def load_library(library, *sub_modules):
    if not library:
        library = DEFAULT_LIBRARY
    return import_module(".".join([library, *sub_modules]))
