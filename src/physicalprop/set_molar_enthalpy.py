from compass_utils import messages
from physicalprop.utils import load_library
from physicalprop.properties import BothProperties


def set_molar_enthalpy_functions(
    physical_prop,
    functions=None,
    *,
    check_derivatives=True,
    library=None,
):
    """
    Set the molar enthalpy functions (with and without the derivatives)
    for all phases.
    If no molar enthalpy function is given, use default values.
    To use another function, give the function
    :param functions: (optional) list of BothProperties (one by phase)
    :param check_derivatives: (True by default) check the derivatives of the property
    """
    if functions is not None:
        messages.warning("overriding default molar enthalpy functions")
    else:
        functions = get_default_molar_enthalpy_functions(physical_prop.physics, library)

    register_c_property_module = BothProperties(
        with_derivatives=physical_prop.cpp_prop_with_derivatives.register_c_molar_enthalpies_with_derivatives,
        without_derivatives=physical_prop.cpp_prop_without_derivatives.register_c_molar_enthalpies_without_derivatives,
    )
    physical_prop.register_property(
        "molar_enthalpy",
        functions,
        register_c_property_module,
        check_derivatives,
    )


def get_default_molar_enthalpy_functions(the_physics, library):
    lib = load_library(library, "enthalpies")
    physics_name = the_physics.name()

    if physics_name == "diphasic":
        assert_diphasic_phase_indexes(the_physics)
        lib.assert_diphasic_components_indexes(the_physics)
        return [
            lib.gas_diphasic_molar_enthalpies,
            lib.liquid_diphasic_molar_enthalpies,
        ]

    elif physics_name == "immiscible2ph":
        assert_diphasic_phase_indexes(the_physics)
        lib.assert_immiscible2ph_components_indexes(the_physics)
        return [
            lib.gas_immiscible2ph_molar_enthalpies,
            lib.liquid_diphasic_molar_enthalpies,
        ]

    elif physics_name == "water2ph":
        assert_diphasic_phase_indexes(the_physics)
        return [lib.gas_water2ph_enthalpies, lib.liquid_water2ph_enthalpies]

    elif physics_name == "linear_water":
        return lib.pure_phase_enthalpies

    elif physics_name == "brine":
        return lib.brine_enthalpies

    else:
        raise "molar enthalpy not implemented for this physics"


def assert_diphasic_phase_indexes(the_physics):
    # assumed in diphasic enthalpies definition
    assert the_physics.Phase.gas == 0
    assert the_physics.Phase.liquid == 1
