#include "./diphasic_bindings.h"
#include "physics/model.h"
#include <cstdint>
#include <nanobind/nanobind.h>
#include <nanobind/ndarray.h>
#include <physicalprop/physicalprop.h>
#include <physicalprop/sanity_check.h>

using namespace physical_prop;
namespace nb = nanobind;
using pp_with_d = Phase_property_with_derivatives_ptr;
using pp_without_d = Phase_property_ptr;
using rock_prop_with_d = Rock_property_with_derivatives_ptr;
using rock_prop_without_d = Rock_property_ptr;
using T_prop_with_d = Temp_property_with_derivatives_ptr;
using T_prop_without_d = Temp_property_ptr;

template <typename PPtr_t, typename Prop_t>
inline void _register_phase_properties(nb::args &args, Prop_t &&prop) {
  if (nb::len(args) != nb_phases)
    throw std::runtime_error("wrong number of phases");
  for (std::size_t k = 0; k < nb_phases; ++k) {
    prop[k] = reinterpret_cast<PPtr_t>(nb::cast<std::uintptr_t>(args[k]));
  }
}

template <typename PyCls> void bind_fmp_class(PyCls &cls) {

  using FMP_t = typename PyCls::Type;
  static_assert(std::is_same_v<FMP_t, FMP> || std::is_same_v<FMP_t, FMP_d>);
  constexpr bool with_derivatives = std::is_same_v<FMP_t, FMP_d>;
  using pp_t = std::conditional_t<with_derivatives, pp_with_d, pp_without_d>;
  auto regname = [](const char *name) {
    if constexpr (with_derivatives)
      return std::string{"register_c_"} + name +
             std::string{"_with_derivatives"};
    else
      return std::string{"register_c_"} + name +
             std::string{"_without_derivatives"};
  };
  cls.def(nb::init<>());
  cls.def(nb::init<const FMP_t &>());
  cls.def_prop_ro(
      "components_molar_mass",
      [](FMP_t &self) {
        return nb::ndarray<nb::numpy, physics::Real, nb::c_contig>(
            self.components_molar_mass.data(),
            {self.components_molar_mass.size()}, nb::handle());
      },
      nb::rv_policy::reference_internal);
  cls.def(regname("viscosities").c_str(), [](FMP_t &self, nb::args args) {
    _register_phase_properties<pp_t>(args, self._dynamic_viscosities);
  });
  cls.def(regname("molar_densities").c_str(), [](FMP_t &self, nb::args args) {
    _register_phase_properties<pp_t>(args, self._molar_densities);
  });
  cls.def(regname("volumetric_mass_densities").c_str(), [](FMP_t &self,
                                                           nb::args args) {
    _register_phase_properties<pp_t>(args, self._volumetric_mass_densities);
  });
  cls.def(regname("molar_enthalpies").c_str(), [](FMP_t &self, nb::args args) {
    _register_phase_properties<pp_t>(args, self._molar_enthalpies);
  });
  cls.def(regname("psat").c_str(), [](FMP_t &self, nb::args args) {
    if (nb::len(args) != 1)
      throw std::runtime_error("error in psat register");
    using Tpp_t =
        std::conditional_t<with_derivatives, T_prop_with_d, T_prop_without_d>;
    self._psat = reinterpret_cast<Tpp_t>(nb::cast<std::uintptr_t>(args[0]));
  });
}

void bind_fluid_physical_properties(nb::module_ &module) {

  auto cls_d = nb::class_<FMP_d>(module, "cppFluidMixturePropWithDeriv",
                                 "Fluid physical properties with derivatives");
  bind_fmp_class(cls_d);
  auto cls = nb::class_<FMP>(module, "cppFluidMixtureProperties",
                             "Fluid physical properties without derivatives");
  bind_fmp_class(cls);
  module.def("cpp_test_visco_no_derivatives", &test_visco_no_derivatives);
  module.def("cpp_test_visco_with_derivatives", &test_visco_with_derivatives);
  module.def("cpp_test_default_psat", &test_default_psat);
}

void bind_rock_physical_properties(nb::module_ &module) {

  auto cls_d = nb::class_<RP_d>(module, "cppRockPropWithDeriv",
                                "Rock physical properties with derivatives");
  cls_d.def(nb::init<>());
  cls_d.def(nb::init<const RP_d &>());
  cls_d.def_rw("rock_heat_capacity", &RP_d::rock_heat_capacity);
  cls_d.def("register_c_rel_perm_with_derivatives",
            [](RP_d &self, nb::args args) {
              // args contains rocktype, phase properties
              if (nb::len(args) != nb_phases + 1)
                throw std::runtime_error("wrong number of phases");
              int rocktype = nb::cast<int>(args[0]);
              // store the rocktype index if it is not already present
              if (std::find(self._rel_perm_rocktypes.begin(),
                            self._rel_perm_rocktypes.end(),
                            rocktype) == self._rel_perm_rocktypes.end())
                self._rel_perm_rocktypes.push_back(rocktype);
              if (size(self._relative_permeabilities) < rocktype + 1)
                self._relative_permeabilities.resize(rocktype + 1);
              auto &&rock_prop_d = self._relative_permeabilities[rocktype];
              for (std::size_t k = 0; k < nb_phases; ++k)
                rock_prop_d[k] = reinterpret_cast<rock_prop_with_d>(
                    nb::cast<std::uintptr_t>(args[k + 1]));
            });
  cls_d.def("register_c_pc_with_derivatives", [](RP_d &self, nb::args args) {
    // args contains rocktype, phase properties
    if (nb::len(args) != nb_phases + 1)
      throw std::runtime_error("wrong number of phases");
    int rocktype = nb::cast<int>(args[0]);
    // store the rocktype index if it is not already present
    if (std::find(self._pc_rocktypes.begin(), self._pc_rocktypes.end(),
                  rocktype) == self._pc_rocktypes.end())
      self._pc_rocktypes.push_back(rocktype);
    if (size(self._pref2palpha) < rocktype + 1)
      self._pref2palpha.resize(rocktype + 1);
    auto &&rock_prop_d = self._pref2palpha[rocktype];
    for (std::size_t k = 0; k < nb_phases; ++k)
      rock_prop_d[k] = reinterpret_cast<rock_prop_with_d>(
          nb::cast<std::uintptr_t>(args[k + 1]));
  });

  auto cls = nb::class_<RP>(module, "cppRockProperties",
                            "Rock physical properties without derivatives");
  cls.def(nb::init<>());
  cls.def(nb::init<const RP &>());
  cls.def_rw("rock_heat_capacity", &RP::rock_heat_capacity);
  cls.def("register_c_rel_perm_without_derivatives",
          [](RP &self, nb::args args) {
            // args contains rocktype, phase properties
            if (nb::len(args) != nb_phases + 1)
              throw std::runtime_error("wrong number of phases");
            int rocktype = nb::cast<int>(args[0]);
            // store the rocktype index if it is not already present
            if (std::find(self._rel_perm_rocktypes.begin(),
                          self._rel_perm_rocktypes.end(),
                          rocktype) == self._rel_perm_rocktypes.end())
              self._rel_perm_rocktypes.push_back(rocktype);
            if (size(self._relative_permeabilities) < rocktype + 1)
              self._relative_permeabilities.resize(rocktype + 1);
            auto &&rock_prop = self._relative_permeabilities[rocktype];
            for (std::size_t k = 0; k < nb_phases; ++k)
              rock_prop[k] = reinterpret_cast<rock_prop_without_d>(
                  nb::cast<std::uintptr_t>(args[k + 1]));
          });
  cls.def("register_c_pc_without_derivatives", [](RP &self, nb::args args) {
    // args contains rocktype, phase properties
    if (nb::len(args) != nb_phases + 1)
      throw std::runtime_error("wrong number of phases");
    int rocktype = nb::cast<int>(args[0]);
    // store the rocktype index if it is not already present
    if (std::find(self._pc_rocktypes.begin(), self._pc_rocktypes.end(),
                  rocktype) == self._pc_rocktypes.end())
      self._pc_rocktypes.push_back(rocktype);
    if (size(self._pref2palpha) < rocktype + 1)
      self._pref2palpha.resize(rocktype + 1);
    if (size(self._pref2palpha) < rocktype + 1)
      self._pref2palpha.resize(rocktype + 1);
    auto &&rock_prop = self._pref2palpha[rocktype];
    for (std::size_t k = 0; k < nb_phases; ++k)
      rock_prop[k] = reinterpret_cast<rock_prop_without_d>(
          nb::cast<std::uintptr_t>(args[k + 1]));
  });
  module.def("cpp_test_rel_perm_no_derivatives", &test_rel_perm_no_derivatives);
  module.def("cpp_test_rel_perm_with_derivatives",
             &test_rel_perm_with_derivatives);
  module.def("cpp_test_default_pc", &test_default_pc);
  module.def("cpp_test_fugacity", &test_fugacity);
  module.def("cpp_test_rock_heat_capacity", &test_rock_heat_capacity);
  module.def("cpp_test_van_genuchten_kr_with_derivatives",
             &test_van_genuchten_kr_with_derivatives);
  module.def("cpp_test_van_genuchten_pc_with_derivatives",
             &test_van_genuchten_pc_with_derivatives);
}

NB_MODULE(bindings, module) {
  bind_fluid_physical_properties(module);
  bind_rock_physical_properties(module);
  add_specific_model_wrappers(module);
}
