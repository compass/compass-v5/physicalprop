from physicalprop import (
    cpp_test_visco_with_derivatives,
    cpp_test_visco_no_derivatives,
)
import physics
from physicalprop.utils import constant_fluid_physical_property
from physicalprop.fluid_physical_properties import FluidMixtureProperties
from physicalprop.set_viscosity import set_viscosity_functions


def cpp_register_visco_test(the_physics):
    def register_diphasic_visco(phy_properties, visco_g, visco_l):
        cst_viscosities = [
            constant_fluid_physical_property(visco_g),
            constant_fluid_physical_property(visco_l),
        ]
        set_viscosity_functions(phy_properties, cst_viscosities)
        return phy_properties

    def test_cpp_values(phy_properties, visco_g, visco_l):
        cpp_test_visco_no_derivatives(
            phy_properties.cpp_prop_without_derivatives,
            visco_g,
            visco_l,
        )
        cpp_test_visco_with_derivatives(
            phy_properties.cpp_prop_with_derivatives,
            visco_g,
            visco_l,
        )

    the_fluid_properties = FluidMixtureProperties(the_physics)
    register_diphasic_visco(the_fluid_properties, 2.0e-5, 1.0e-3)
    test_cpp_values(the_fluid_properties, 2.0e-5, 1.0e-3)
    register_diphasic_visco(the_fluid_properties, 5.0e-5, 3.0e-3)
    test_cpp_values(the_fluid_properties, 5.0e-5, 3.0e-3)


def test_cpp_call():

    # load the physics
    diphasic_physics = physics.load("diphasic")
    cpp_register_visco_test(diphasic_physics)
